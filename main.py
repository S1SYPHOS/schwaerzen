import io
import os
import re
import glob

import yaml
import pdf_redactor
import sitzungsdienst


# Load database
with open('names.yml', 'r') as file:
    names = yaml.safe_load(file)


def get_first_name(match: re.Match) -> str:
    """
    :param match: re.Match Matched string

    :return: str Different first name
    """

    return names['first'][match.group(0)]


def get_last_name(match: re.Match) -> str:
    """
    :param match: re.Match Matched string

    :return: str Different last name
    """

    return names['last'][match.group(0)]


def random_docket(match: re.Match) -> str:
    """
    Creates random docket number

    :param match: re.Match Matched string

    :return: str Random docket number
    """

    from random import randint

    return '{} Js {}/{}'.format(randint(10, 99), randint(12345, 56789), randint(19, 22))


def check(string: str, names: list) -> bool:
    """
    Checks first names

    :param string: str Name to be tested
    :param names: list Names in database

    :return: bool
    :raises: Exception Missing replacement
    """

    for name, replacement in names.items():
        if name == replacement:
            raise Exception('Missing replacement: "{}"'.format(name))

        if name.lower() in string.lower():
            return True

    return False


if __name__ == '__main__':
    # Define message
    msg = 'All systems check out.'

    # Attempt to ..
    try:
        # .. loop over PDF files
        for pdf_file in glob.glob('src/*.pdf'):
            sta = sitzungsdienst.Sitzungsdienst(pdf_file)

            for name in set(sta.extract_people()):
                # Skip placeholders
                if 'NN Sitzungsvertreter' in name:
                    continue

                # Check ..
                # (1) .. first names
                if not check(name, names['first']):
                    raise Exception('Missing first name: "{}"'.format(name))

                # (2) .. last names
                if not check(name, names['last']):
                    raise Exception('Missing last name: "{}"'.format(name))

            # Prepare files
            filename = os.path.basename(pdf_file)
            output_file = 'dist/' + filename

            # If output file already exists ..
            if os.path.exists(output_file):
                # .. move on
                continue

            options = pdf_redactor.RedactorOptions()

            with open(pdf_file, 'rb') as file:
                input_stream = io.BytesIO(file.read())

            output_stream = io.BytesIO()

            options.input_stream = input_stream
            options.output_stream = output_stream

            # Clear any XMP metadata, if present.
            options.xmp_filters = [lambda xml : None]

            # Replace ..
            # (1) .. first names
            for first, replacement in names['first'].items():
                options.content_filters.append((re.compile(first), get_first_name))

            # (2) .. last names
            for last, replacement in names['last'].items():
                options.content_filters.append((re.compile(last), get_last_name))

            # (3) .. dockets
            options.content_filters.append((re.compile(r'\d{2}\sU?Js\s\d+\/\d{2}'), random_docket))

            pdf_redactor.redactor(options)

            output_stream.seek(0)

            with open(output_file, 'wb') as file:
                file.write(output_stream.getbuffer())

    except Exception as e:
        # Store error
        msg = e

    print(msg)
